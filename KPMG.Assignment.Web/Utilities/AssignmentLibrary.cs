﻿using System.Globalization;
using System.Linq;

namespace KPMG.Assignment.Web.Utilities
{
    public static class AssignmentLibrary
    {
        public static bool IsValidISOCurrencyCode(string currencyCode)
        {
            return CultureInfo.GetCultures(CultureTypes.AllCultures).Where(c =>
                    !c.IsNeutralCulture).Select(culture =>
            {
                try
                {
                    return new RegionInfo(culture.LCID);
                }
                catch
                {
                    return null;
                }
            }).Any(ri => (ri != null) && (ri.ISOCurrencySymbol == currencyCode));
        }

        public static bool IsValidDecimal(string amount)
        {
            decimal number;
            return decimal.TryParse(amount, out number);
        }
    }
}