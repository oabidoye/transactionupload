﻿using System.Collections.Generic;

namespace KPMG.Assignment.Web.Models
{
    public class ContentAddedViewModel
    {
        public int NumberOfLines { get; set; }
        public int NumberOfImportedLines { get; set; }

        public Dictionary<int, string> FailedLines { get; set; }
    }
}