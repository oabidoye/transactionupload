﻿using System;
using System.ComponentModel.DataAnnotations;
using KPMG.Assignment.Web.Utilities;

namespace KPMG.Assignment.Web.Models
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class UsableCurrencyCode : ValidationAttribute
    {
        public UsableCurrencyCode(string errorMessage) : base(errorMessage)
        {
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var validationResult = ValidationResult.Success;

            if (!AssignmentLibrary.IsValidISOCurrencyCode(value.ToString().ToUpper()))
                validationResult = new ValidationResult(ErrorMessageString);

            return validationResult;
        }
    }
}