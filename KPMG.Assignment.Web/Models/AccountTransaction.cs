﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace KPMG.Assignment.Web.Models
{
    public class AccountTransaction
    {
        private string _currencyCode;

        [Key]
        public int TransactionId { get; set; }

        [Required(ErrorMessage = "Account is required!")]
        public string Account { get; set; }

        [Required(ErrorMessage = "Description is required!")]
        public string Description { get; set; }

        [DisplayName("Currency Code")]
        [Required(ErrorMessage = "Currency code is required!")]
        [UsableCurrencyCode("Valid currency code is required!")]
        public string CurrencyCode
        {
            get { return string.IsNullOrEmpty(_currencyCode) ? _currencyCode : _currencyCode.ToUpper(); }
            set { _currencyCode = value; }
        }

        [Required(ErrorMessage = "Valid amount is required!")]
        public decimal Amount { get; set; }

        [DisplayName("Transaction Date")]
        public DateTime TransactionDate { get; set; }

        public bool Active { get; set; }
    }
}