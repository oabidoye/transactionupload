﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Web;

namespace KPMG.Assignment.Web.Models
{
    public class UploadViewModel
    {
        [DisplayName("Upload a valid transaction file")]
        [Required(ErrorMessage = "Valid transaction file is required!")]
        public HttpPostedFileBase TransactionFile { get; set; }

        public DataTable TransactionTable { get; set; }

        public string FileText { get; set; }
    }
}