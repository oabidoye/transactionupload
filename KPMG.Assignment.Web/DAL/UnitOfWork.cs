﻿using System;
using KPMG.Assignment.Web.Models;

namespace KPMG.Assignment.Web.DAL
{
    public class UnitOfWork : IDisposable
    {
        private readonly TransactionContext _context = new TransactionContext();
        private Repository<AccountTransaction> _accountTransactionRepository;

        private bool _disposed;

        public Repository<AccountTransaction> AccountTransactionRepository
        {
            get
            {
                return _accountTransactionRepository ??
                       (_accountTransactionRepository = new Repository<AccountTransaction>(_context));
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
                if (disposing)
                    _context.Dispose();
            _disposed = true;
        }
    }
}