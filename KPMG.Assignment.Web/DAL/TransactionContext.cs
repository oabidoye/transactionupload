﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using KPMG.Assignment.Web.Models;

namespace KPMG.Assignment.Web.DAL
{
    public class TransactionContext : DbContext
    {
        public TransactionContext() : base("TransactionContext")
        {
        }

        public DbSet<AccountTransaction> AccountTransactions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}