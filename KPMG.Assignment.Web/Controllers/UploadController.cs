﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Excel;
using KPMG.Assignment.Web.DAL;
using KPMG.Assignment.Web.Models;
using KPMG.Assignment.Web.Utilities;
using LumenWorks.Framework.IO.Csv;

namespace KPMG.Assignment.Web.Controllers
{
    public class UploadController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();

        public ActionResult Index()
        {
            return View(new UploadViewModel());
        }

        public ActionResult AddFileContent()
        {
            var viewModel = new ContentAddedViewModel();
            var dt = TempData["TransactionTable"] as DataTable;

            return View(dt == null ? viewModel : AddOrUpdateAccountTansaction(dt));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(UploadViewModel uploadViewModel)
        {
            if (ModelState.IsValid)
            {
                if ((uploadViewModel.TransactionFile == null) || (uploadViewModel.TransactionFile.ContentLength <= 0))
                {
                    ModelState.AddModelError("File",
                        "Please upload a valid file. The format must be in Excel (xlsx) or CSV.");
                    return View(uploadViewModel);
                }

                var stream = uploadViewModel.TransactionFile.InputStream;

                if (uploadViewModel.TransactionFile.FileName.EndsWith(".xlsx"))
                {
                    using (var excelDataReader = ExcelReaderFactory.CreateOpenXmlReader(stream))
                    {
                        excelDataReader.IsFirstRowAsColumnNames = true;
                        var resultDataSet = excelDataReader.AsDataSet();
                        uploadViewModel.TransactionTable = resultDataSet.Tables[0];
                    }
                }
                else if (uploadViewModel.TransactionFile.FileName.EndsWith(".csv"))
                {
                    using (var csvReader = new CsvReader(new StreamReader(stream), true))
                    {
                        uploadViewModel.TransactionTable = new DataTable();
                        uploadViewModel.TransactionTable.Load(csvReader);
                    }
                }

                else
                {
                    ModelState.AddModelError("File",
                        "The uploaded file format is not supported. The format must be in Excel (xlsx) or CSV.");

                    return View(uploadViewModel);
                }

                TempData["TransactionTable"] = uploadViewModel.TransactionTable;

                return View(uploadViewModel);
            }

            ModelState.AddModelError("File", "Please upload a valid file. The format must be in Excel (xlsx) or CSV.");
            return View(uploadViewModel);
        }

        private static bool TryValidate(object accountTransaction, out List<ValidationResult> results)
        {
            var context = new ValidationContext(accountTransaction);
            results = new List<ValidationResult>();

            return Validator.TryValidateObject(accountTransaction, context, results);
        }

        private ContentAddedViewModel AddOrUpdateAccountTansaction(DataTable dataTable)
        {
            var contentAddedViewModel = new ContentAddedViewModel();
            var lineNumber = 1;

            contentAddedViewModel.FailedLines = new Dictionary<int, string>();
            contentAddedViewModel.NumberOfLines = dataTable.Rows.Count;

            foreach (DataRow dataRow in dataTable.Rows)
            {
                try
                {
                    var amountText = dataRow["Amount"].ToString();
                    if (AssignmentLibrary.IsValidDecimal(amountText))
                    {
                        var accountTransaction = new AccountTransaction
                        {
                            Account = dataRow["Account"].ToString(),
                            Description = dataRow["Description"].ToString(),
                            CurrencyCode = dataRow["Currency Code"].ToString(),
                            Amount = Convert.ToDecimal(amountText),
                            TransactionDate = DateTime.Now,
                            Active = true
                        };

                        List<ValidationResult> results;

                        if (TryValidate(accountTransaction, out results))
                        {
                            _unitOfWork.AccountTransactionRepository.Insert(accountTransaction);
                            contentAddedViewModel.NumberOfImportedLines++;
                        }
                        else
                        {
                            contentAddedViewModel.FailedLines.Add(lineNumber,
                                string.Join(". ", results.Select(e => e.ErrorMessage)));
                        }
                    }
                    else
                    {
                        contentAddedViewModel.FailedLines.Add(lineNumber,
                            "Valid amount is required - Amount must be a valid number!");
                    }
                }
                catch (Exception ex)
                {
                    contentAddedViewModel.FailedLines.Add(lineNumber, ex.Message);
                }
                lineNumber++;
            }
            try
            {
                _unitOfWork.Save();
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
            }

            return contentAddedViewModel;
        }
    }
}