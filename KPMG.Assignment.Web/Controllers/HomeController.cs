﻿using System.Web.Mvc;

namespace KPMG.Assignment.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}