﻿using System;
using System.Linq;
using System.Web.Mvc;
using KPMG.Assignment.Web.DAL;
using KPMG.Assignment.Web.Models;

namespace KPMG.Assignment.Web.Controllers
{
    public class AccountTransactionController : Controller
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();

        // GET: AccountTransactions
        public ActionResult Index()
        {
            return View(_unitOfWork.AccountTransactionRepository.Get().ToList());
        }

        // GET: AccountTransactions/Details/5
        public ActionResult Details(int id)
        {
            var accountTransaction = _unitOfWork.AccountTransactionRepository.GetById(id);
            if (accountTransaction == null)
                return HttpNotFound();
            return View(accountTransaction);
        }

        // GET: AccountTransactions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AccountTransactions/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(
            [Bind(Include = "TransactionId,Account,Description,CurrencyCode,Amount,TransactionDate")] AccountTransaction
                accountTransaction)
        {
            if (ModelState.IsValid)
            {
                accountTransaction.TransactionDate = DateTime.Now;
                accountTransaction.Active = true;

                _unitOfWork.AccountTransactionRepository.Insert(accountTransaction);
                _unitOfWork.Save();

                return RedirectToAction("Index");
            }
            return View(accountTransaction);
        }

        // GET: AccountTransactions/Edit/5
        public ActionResult Edit(int id)
        {
            var accountTransaction = _unitOfWork.AccountTransactionRepository.GetById(id);
            if (accountTransaction == null)
                return HttpNotFound();
            return View(accountTransaction);
        }

        // POST: AccountTransactions/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(
            [Bind(Include = "TransactionId,Account,Description,CurrencyCode,Amount,TransactionDate,Active")] AccountTransaction accountTransaction)
        {
            if (!ModelState.IsValid) return View(accountTransaction);

            accountTransaction.TransactionDate = DateTime.Now;
            _unitOfWork.AccountTransactionRepository.Update(accountTransaction);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: AccountTransactions/Delete/5
        public ActionResult Delete(int id)
        {
            var accountTransaction = _unitOfWork.AccountTransactionRepository.GetById(id);
            if (accountTransaction == null)
                return HttpNotFound();
            return View(accountTransaction);
        }

        // POST: AccountTransactions/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var accountTransaction = _unitOfWork.AccountTransactionRepository.GetById(id);
            accountTransaction.TransactionDate = DateTime.Now;
            accountTransaction.Active = false;

            _unitOfWork.AccountTransactionRepository.Update(accountTransaction);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}